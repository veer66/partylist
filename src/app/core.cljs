(ns app.core
  (:require
    [clojure.edn :as edn]
    [uix.core :as uix :refer [defui $ use-state]]
    [uix.dom]
    [shadow.resource :as rc]
    ["@silevis/reactgrid" :refer [ReactGrid]]))

(def columns
  [{:columnId "name" :width 200}
   {:columnId "score" :width 100}])

(def header {:rowId "header"
             :cells [{:type "header" :text "ชื่อ"}
                     {:type "header" :text "คะแนน"}]})

(defn รวมคะแนน [รายการพรรค]
  (reduce (fn [คะแนน พรรค]
            (+ คะแนน (:score พรรค)))
          0
          รายการพรรค))

(defn cal-avg-score-per-member [คะแนนรวม]
  (/ (double คะแนนรวม) 100.0))

(defn คำนวณ [รายการพรรค]
  (let [คะแนนรวม (รวมคะแนน รายการพรรค)
        avg (cal-avg-score-per-member คะแนนรวม)]
    (->> รายการพรรค
         (map (fn [พรรค]
                (assoc พรรค :คะแนนต่อคะแนนเฉลี่ย
                       (/ (double (:score พรรค))
                          avg))))
         (map (fn [พรรค]
                (assoc พรรค :คะแนนต่อคะแนนเฉลี่ยปัดเศษทิ้ง
                       (int (Math/floor (:คะแนนต่อคะแนนเฉลี่ย พรรค))))))
         (map (fn [พรรค]
                (assoc พรรค :คะแนนเศษ
                       (- (:คะแนนต่อคะแนนเฉลี่ย พรรค)
                          (:คะแนนต่อคะแนนเฉลี่ยปัดเศษทิ้ง พรรค))))))))

(defn party->row [i party]
  {:rowId i
   :cells [{:type "text"
            :text (:name party)}
           {:type "number"
            :value (:score party)}]})

(defn to-rows [parties]
  (concat [header]
          (map-indexed party->row parties)))

(defn update-row [row-id value row]
  (if (= row-id (:rowId row))
    (assoc row :value value)
    row))

(defn changes-to-row-id-val [changes]
  (loop [changes (js->clj changes)
         m (transient {})]
    (if (empty? changes)
      (persistent! m)
      (recur (rest changes)
             (let [change (first changes)
                   row-id (get change "rowId")
                   new-cell (get change "newCell")
                   val (get new-cell "value")]
               (assoc! m row-id val))))))

(defn update-parties [parties change-map]
  (map-indexed (fn [row-id party]
                 (if-let [val (get change-map row-id)]
                   (assoc party :score val)
                   party))
               parties))

(defn on-score-changed [parties set-parties! changes]
  (let [change-map (changes-to-row-id-val changes)]
    (set-parties! (คำนวณ (update-parties parties change-map)))))

(defn init-parties []
  (let [parties (->> (rc/inline "parties.edn")
                     edn/read-string
                     (into []))]
    (คำนวณ parties)))

(defn float-format [f]
  (/ (Math/round (* 100.0 f)) 100.0))

(defui app []
  (let [[parties set-parties!] (use-state (init-parties))]
    ($ :.app
       ($ :h1 "คำนวณจำนวนส.ส.บัญชีรายชื่อ 66")
       ($ :p "เว็บอยู่ในระหว่างพัฒนาการคำนวณอาจมีข้อผิดพลาดหรือยังไม่ถูกต้องสมบูรณ์ตาม")
       ($ :p "คะแนนทั้งหมดสมมุติขึ้นมาเพื่อเป็นตัวอย่างในการคำนวณไม่เกี่ยวข้องกับคะแนนจริง")
       ($ :a {:href "https://ratchakitcha.soc.go.th/pdfdownload/?id=140A007N0000000000100"} "พระราชบัญญัติประกอบรัฐธรรมนูญ 2566")
       ($ :p "ช่วยปรับปรุงโปรแกรมได้ที่ " ($ :a {:href "https://codeberg.org/veer66/partylist"} "https://codeberg.org/veer66/partylist"))
       ($ :div.container
          ($ :div.col
             ($ :h3 "คะแนนบัญชีรายชื่อ")
             ($ ReactGrid {:rows (clj->js (to-rows parties))
                           :columns (clj->js columns)
                           :onCellsChanged #(on-score-changed parties set-parties! %)}))
          ($ :div.col
             (let [คะแนนรวม (รวมคะแนน parties)
                   avg (cal-avg-score-per-member คะแนนรวม)
                   num-of-members-๑๒๘-๓ (reduce (fn [จำนวน พรรค]
                                                  (+ (:คะแนนต่อคะแนนเฉลี่ยปัดเศษทิ้ง พรรค)
                                                     จำนวน))
                                                0 parties)
                   num-of-members-๑๒๘-๔ (- 100 num-of-members-๑๒๘-๓)]
               ($ :div
                  ($ :div
                     ($ :h3 "คะแนนรวม")
                     ($ :span คะแนนรวม))
                  ($ :div
                     ($ :h3 "คะแนนเฉลี่ย")
                     ($ :span avg))
                  ($ :div
                     ($ :h3 "คะแนนเสียง / คะแนนเฉลี่ย")
                     ($ :ul
                        (for [พรรค (->> parties
                                        (filter #(>= (:คะแนนต่อคะแนนเฉลี่ย %) 1.0))
                                        (sort-by :คะแนนต่อคะแนนเฉลี่ย >))]
                          ($ :li
                             (:name พรรค)
                             ": "
                             (float-format (:คะแนนต่อคะแนนเฉลี่ย พรรค))
                             " ปัดเศษทิ้งแล้วเหลือ: "
                             (:คะแนนต่อคะแนนเฉลี่ยปัดเศษทิ้ง พรรค)))))
                  ($ :div
                     ($ :h3 "จำนวนสมาชิกรวมตามม.๑๒๘ ข้อ ๓")
                     ($ :span num-of-members-๑๒๘-๓))
                  ($ :div
                     ($ :h3 "จำนวนสมาชิกตามม.๑๒๘ ข้อ ๔")
                     ($ :span num-of-members-๑๒๘-๔))
                  ($ :div
                     ($ :h3 "พรรคที่ได้สมาชิกตามม.๑๒๘ ข้อ ๔")
                     ($ :ol
                        (for [พรรค (take num-of-members-๑๒๘-๔ (sort-by :คะแนนเศษ > parties))]
                          ($ :li (:name พรรค)
                             ", โดยมีคะแนนเศษ: "
                             (float-format (:คะแนนเศษ พรรค)))))))))))))

(defonce root
  (uix.dom/create-root (js/document.getElementById "root")))

(defn render []
  (uix.dom/render-root ($ app) root))

(defn ^:export init []
  (render))
